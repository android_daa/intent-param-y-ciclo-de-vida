package cl.duoc.intent;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SegundaActivity extends Activity {

    private TextView txtValorRecibido;
    private final String TAG = "ciclo_activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        txtValorRecibido = (TextView) findViewById(R.id.txtValorRecibido);
        String valor = getIntent().getStringExtra("valor");
        if (valor != null) {
            txtValorRecibido.setText(valor);
        }
        Log.e(TAG, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    /*
    onCreate(Bundle): Se llama en la creación de la actividad. Se utiliza para realizar inicializaciones,
    como la creación de la interfaz de usuario o la inicialización de estructuras de datos.
    Puede recibir información de estado dela actividad (en una instancia de la clase Bundle),
    por si se reanuda desde una actividad que ha sido destruida y vuelta a crear.

    onStart(): Nos indica que la actividad está a punto de ser mostrada al usuario.

    onResume(): Se llama cuando la actividad va a comenzar a interactuar con el usuario. Es un buen lugar para lanzar las animaciones y la música.

    onPause(): Indica que la actividad está a punto de ser lanzada a segundo plano, normalmente porque otra actividad es lanzada. Es el lugar adecuado para detener animaciones, música o almacenar los datos que estaban en edición.

    onStop(): La actividad ya no va a ser visible para el usuario. Ojo si hay muy poca memoria, es posible que la actividad se destruya sin llamar a este método.

    onRestart(): Indica que la actividad va a volver a ser representada después de haber pasado por onStop().

    onDestroy(): Se llama antes de que la actividad sea totalmente destruida. Por ejemplo, cuando el usuario pulsa el botón de volver o cuando se llama al método finish(). Ojo si hay muy poca memoria, es posible que la actividad se destruya sin llamar a este método.
    * */
}
