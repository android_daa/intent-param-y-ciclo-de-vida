package cl.duoc.intent;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class PrimeraActivity extends ActionBarActivity {

    private EditText txtValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primera);
        txtValor = (EditText)findViewById(R.id.txtValor);
    }

    public void onClickEntrar(View v){
        Intent i = new Intent(this, SegundaActivity.class);
        i.putExtra("valor", txtValor.getText().toString());
        startActivity(i);
    }
}
